# TYUIKitAdditions
Additional categories for the UIKit framework

## How To Use

```objective-c
#import <TYUIKitAdditions/TYUIKitAdditions.h>
...
```

### 文件列表: 
* UIApplication+TYAdditions.h
* UIDevice+TYAdditions.h
* UIScreen+TYAdditions.h
* UISearchBar+TYAdditions.h
* UITableView+TYAdditions.h
* UITableViewCell+TYAdditions.h
* UIView+TYAdditions.h
* UIView+TYActivityIndicator.h
* UIView+TYBadgePoint.h
* UIImage+TYAdditions.h
* UICollectionViewCell+TYAdditions.h
* UIBarButtonItem+TYAdditions.h
* UIAlertView+TYAdditions.h
* TYTableViewCellProtocol.h
* UIActionSheet+TYAdditions.h
* UIActionSheet+TYFront.h
