//
//  UIImage+TYAdditions.m
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/11.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIImage+TYAdditions.h"

@implementation UIImage (TYAdditions)

- (NSData *)ty_dataSmallerThan:(NSUInteger)dataLength {
    NSData *data = UIImageJPEGRepresentation(self, 1.0);
    while (data.length > dataLength) {
        UIImage *image = [UIImage imageWithData:data];
        data = UIImageJPEGRepresentation(image, 0.7);
    }
    return data;
}

- (NSData *)ty_dataForUpload {
    return [self ty_dataSmallerThan:1024 * 1000];
}

@end
