//
//  UIAlertView+TYAdditions.m
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/27.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIAlertView+TYAdditions.h"
#import <BlocksKit/UIAlertView+BlocksKit.h>

@implementation UIAlertView (TYAdditions)

+ (void)ty_showAlertTip:(NSString *)msg {
    UIAlertView *alertView = [UIAlertView bk_showAlertViewWithTitle:@"提示" message:msg cancelButtonTitle:@"知道了" otherButtonTitles:nil handler:nil];
    [alertView show];
}

@end
