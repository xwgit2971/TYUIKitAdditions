//
//  UIView+TYAdditions.m
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIView+TYAdditions.h"
#import <UIColor-TYAdditions/UIColor+TYAdditions.h>
#import "UIScreen+TYAdditions.h"

#define kTagLineView 1007

#define kColorDDD [UIColor add_colorWithRGBHexString:@"0xDDDDDD"]

@implementation UIView (TYAdditions)

- (CGSize)ty_doubleSizeOfFrame {
    CGSize size = self.frame.size;
    return CGSizeMake(size.width * 2, size.height * 2);
}

- (void)ty_removeViewWithTag:(NSInteger)tag{
    for (UIView *aView in [self subviews]) {
        if (aView.tag == tag) {
            [aView removeFromSuperview];
        }
    }
}

@end


@implementation UIView (TYNib)

+ (instancetype)ty_viewFromNib {
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    if (views.count > 0) {
        return views.firstObject;
    }
    return nil;
}

@end


@implementation UIView (TYLayer)
@dynamic ty_borderColor, ty_borderWidth, ty_cornerRadius, ty_masksToBounds;

- (void)setMm_borderColor:(UIColor *)ty_borderColor {
    [self.layer setBorderColor:ty_borderColor.CGColor];
}

- (void)setMm_borderWidth:(CGFloat)ty_borderWidth {
    [self.layer setBorderWidth:ty_borderWidth];
}

- (void)setMm_cornerRadius:(CGFloat)ty_cornerRadius {
    [self.layer setCornerRadius:ty_cornerRadius];
}

- (void)setMm_masksToBounds:(BOOL)ty_masksToBounds {
    [self.layer setMasksToBounds:ty_masksToBounds];
}

- (void)ty_doCircleFrame {
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.frame.size.width/2;
}

- (void)ty_doNotCircleFrame {
    self.layer.cornerRadius = 0.0;
    self.layer.borderWidth = 0.0;
}

- (void)ty_doBorderWidth:(CGFloat)width color:(UIColor *)color cornerRadius:(CGFloat)cornerRadius {
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = cornerRadius;
    self.layer.borderWidth = width;
    if (!color) {
        self.layer.borderColor = kColorDDD.CGColor;
    } else {
        self.layer.borderColor = color.CGColor;
    }
}

- (void)ty_addRoundingCorners:(UIRectCorner)corners cornerRadii:(CGSize)cornerRadii {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:cornerRadii];
    CAShapeLayer *maskLayer = [CAShapeLayer new];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

- (void)ty_addGradientLayerWithColors:(NSArray *)cgColorArray locations:(NSArray *)floatNumArray startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = self.bounds;
    if (cgColorArray && [cgColorArray count] > 0) {
        layer.colors = cgColorArray;
    } else {
        return;
    }
    if (floatNumArray && [floatNumArray count] == [cgColorArray count]) {
        layer.locations = floatNumArray;
    }
    layer.startPoint = startPoint;
    layer.endPoint = endPoint;
    [self.layer addSublayer:layer];
}

@end


@implementation UIView (TYScroll)

- (UIView *)ty_scrollViewContainer {
    UIView *scrollViewContainer = self;
    for (UIView *aView in [self subviews]) {
        if ([aView isKindOfClass:[UITableView class]]) {
            scrollViewContainer = aView;
        }
    }
    return scrollViewContainer;
}

- (void)ty_setSubScrollsToTop:(BOOL)scrollsToTop {
    [[self subviews] enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIScrollView class]]) {
            [(UIScrollView *)obj setScrollEnabled:scrollsToTop];
            *stop = YES;
        }
    }];
}

@end


@implementation UIView (TYViewController)

- (UIViewController *)ty_findViewController {
    for (UIView *next = self; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

@end


@implementation UIView (TYAnimation)

+ (UIViewAnimationOptions)ty_animationOptionsForCurve:(UIViewAnimationCurve)curve {
    switch (curve) {
        case UIViewAnimationCurveEaseInOut:
            return UIViewAnimationOptionCurveEaseInOut;
        case UIViewAnimationCurveEaseIn:
            return UIViewAnimationOptionCurveEaseIn;
        case UIViewAnimationCurveEaseOut:
            return UIViewAnimationOptionCurveEaseOut;
        case UIViewAnimationCurveLinear:
            return UIViewAnimationOptionCurveLinear;
    }

    return kNilOptions;
}

@end


@implementation UIView (TYLine)

#pragma mark - 直线
- (void)ty_addLineUp:(BOOL)hasUp andDown:(BOOL)hasDown {
    [self ty_addLineUp:hasUp andDown:hasDown andColor:kColorDDD];
}

- (void)ty_addLineUp:(BOOL)hasUp andDown:(BOOL)hasDown andColor:(UIColor *)color {
    [self ty_removeViewWithTag:kTagLineView];
    if (hasUp) {
        UIView *upView = [UIView ty_lineViewWithPointYY:0 andColor:color];
        upView.tag = kTagLineView;
        [self addSubview:upView];
    }
    if (hasDown) {
        UIView *downView = [UIView ty_lineViewWithPointYY:CGRectGetMaxY(self.bounds)-0.5 andColor:color];
        downView.tag = kTagLineView;
        [self addSubview:downView];
    }
    return [self ty_addLineUp:hasUp andDown:hasDown andColor:color andLeftSpace:0];
}

- (void)ty_addLineUp:(BOOL)hasUp andDown:(BOOL)hasDown andLeftSpace:(CGFloat)leftSpace {
    [self ty_addLineUp:hasUp andDown:hasDown andColor:kColorDDD andLeftSpace:leftSpace];
}

- (void)ty_addLineUp:(BOOL)hasUp andDown:(BOOL)hasDown andColor:(UIColor *)color andLeftSpace:(CGFloat)leftSpace {
    [self ty_removeViewWithTag:kTagLineView];
    if (hasUp) {
        UIView *upView = [UIView ty_lineViewWithPointYY:0 andColor:color andLeftSpace:leftSpace];
        upView.tag = kTagLineView;
        [self addSubview:upView];
    }
    if (hasDown) {
        UIView *downView = [UIView ty_lineViewWithPointYY:CGRectGetMaxY(self.bounds)-0.5 andColor:color andLeftSpace:leftSpace];
        downView.tag = kTagLineView;
        [self addSubview:downView];
    }
}

+ (UIView *)ty_lineViewWithPointYY:(CGFloat)pointY {
    return [self ty_lineViewWithPointYY:pointY andColor:kColorDDD];
}

+ (UIView *)ty_lineViewWithPointYY:(CGFloat)pointY andLeftSpace:(CGFloat)leftSpace {
    return [self ty_lineViewWithPointYY:pointY andColor:kColorDDD andLeftSpace:leftSpace];
}

+ (UIView *)ty_lineViewWithPointYY:(CGFloat)pointY andColor:(UIColor *)color {
    return [self ty_lineViewWithPointYY:pointY andColor:color andLeftSpace:0];
}

+ (UIView *)ty_lineViewWithPointYY:(CGFloat)pointY andColor:(UIColor *)color andLeftSpace:(CGFloat)leftSpace {
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(leftSpace, pointY, [UIScreen ty_width] - leftSpace, 0.5)];
    lineView.backgroundColor = color;
    return lineView;
}

@end


@implementation UIView (TYDebug)

- (void)ty_outputSubviewTree {
    [UIView ty_outputTreeInView:self withSeparatorCount:0];
}

+ (void)ty_outputTreeInView:(UIView *)view withSeparatorCount:(NSInteger)count {
    NSString *outputStr = @"";
    outputStr = [outputStr stringByReplacingCharactersInRange:NSMakeRange(0, count) withString:@"-"];
    outputStr = [outputStr stringByAppendingString:view.description];
    printf("%s\n", outputStr.UTF8String);

    if (view.subviews.count == 0) {
        return;
    } else {
        count++;
        for (UIView *subV in view.subviews) {
            [self ty_outputTreeInView:subV withSeparatorCount:count];
        }
    }
}

@end
