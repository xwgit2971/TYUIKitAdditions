//
//  UICollectionViewCell+TYAdditions.h
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/19.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (TYAdditions)

// 复用Identifier
+ (NSString *)ty_cellReuseIdentifier;
// 生成nib
+ (UINib *)ty_nib;

@end
