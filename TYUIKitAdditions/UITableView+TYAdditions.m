//
//  UITableView+TYAdditions.m
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/26.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UITableView+TYAdditions.h"
#import <UIColor-TYAdditions/UIColor+TYAdditions.h>

#define kColorDDD [UIColor add_colorWithRGBHexString:@"0xDDDDDD"]

@implementation UITableView (TYAdditions)

- (void)ty_addLineforPlainCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withLeftSpaceAndSectionLine:(CGFloat)leftSpace {
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];

    CGMutablePathRef pathRef = CGPathCreateMutable();

    CGRect bounds = CGRectInset(cell.bounds, 0, 0);

    CGPathAddRect(pathRef, nil, bounds);

    layer.path = pathRef;

    CFRelease(pathRef);
    if (cell.backgroundColor) {
        // layer的填充色用cell原本的颜色
        layer.fillColor = cell.backgroundColor.CGColor;
    } else if (cell.backgroundView && cell.backgroundView.backgroundColor) {
        layer.fillColor = cell.backgroundView.backgroundColor.CGColor;
    } else {
        layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
    }
    CGColorRef lineColor = kColorDDD.CGColor;

    // 判断整个tableview 最后的元素
    if ((self.numberOfSections == (indexPath.section + 1)) && indexPath.row == [self numberOfRowsInSection:indexPath.section] - 1) {
        // 上短,下长
        [self layer:layer addLineUp:NO andLong:YES andColor:lineColor andBounds:bounds withLeftSpace:0];
    } else {
        [self layer:layer addLineUp:NO andLong:NO andColor:lineColor andBounds:bounds withLeftSpace:leftSpace];
    }

    UIView *testView = [[UIView alloc] initWithFrame:bounds];
    [testView.layer insertSublayer:layer atIndex:0];
    cell.backgroundView = testView;
}

- (void)ty_addLineforPlainCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withLeftSpace:(CGFloat)leftSpace withColor:(UIColor *)color {
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];

    CGMutablePathRef pathRef = CGPathCreateMutable();

    CGRect bounds = CGRectInset(cell.bounds, 0, 0);

    CGPathAddRect(pathRef, nil, bounds);

    layer.path = pathRef;

    CFRelease(pathRef);
    if (cell.backgroundColor) {
        // layer的填充色用cell原本的颜色
        layer.fillColor = cell.backgroundColor.CGColor;
    } else if (cell.backgroundView && cell.backgroundView.backgroundColor) {
        layer.fillColor = cell.backgroundView.backgroundColor.CGColor;
    } else {
        layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
    }
    CGColorRef lineColor = color.CGColor;

    // 判断整个tableview 最后的元素
    if ((self.numberOfSections == (indexPath.section + 1)) && indexPath.row == [self numberOfRowsInSection:indexPath.section] - 1) {
        // 上短,下长
        [self layer:layer addLineUp:NO andLong:YES andColor:lineColor andBounds:bounds withLeftSpace:0];
    } else {
        [self layer:layer addLineUp:NO andLong:NO andColor:lineColor andBounds:bounds withLeftSpace:leftSpace];
    }

    UIView *testView = [[UIView alloc] initWithFrame:bounds];
    [testView.layer insertSublayer:layer atIndex:0];
    cell.backgroundView = testView;
}

- (void)ty_addLineforPlainCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withLeftSpace:(CGFloat)leftSpace hasSectionLine:(BOOL)hasSectionLine {
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];

    CGMutablePathRef pathRef = CGPathCreateMutable();

    CGRect bounds = CGRectInset(cell.bounds, 0, 0);

    CGPathAddRect(pathRef, nil, bounds);

    layer.path = pathRef;

    CFRelease(pathRef);
    if (cell.backgroundColor) {
        // layer的填充色用cell原本的颜色
        layer.fillColor = cell.backgroundColor.CGColor;
    } else if (cell.backgroundView && cell.backgroundView.backgroundColor) {
        layer.fillColor = cell.backgroundView.backgroundColor.CGColor;
    } else {
        layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
    }

    CGColorRef lineColor = kColorDDD.CGColor;
    CGColorRef sectionLineColor = lineColor;

    if (indexPath.row == 0 && indexPath.row == [self numberOfRowsInSection:indexPath.section] - 1) {
        // 只有一个cell。加上长线&下长线
        if (hasSectionLine) {
            [self layer:layer addLineUp:YES andLong:YES andColor:sectionLineColor andBounds:bounds withLeftSpace:0];
            [self layer:layer addLineUp:NO andLong:YES andColor:sectionLineColor andBounds:bounds withLeftSpace:0];
        }
    } else if (indexPath.row == 0) {
        // 第一个cell。加上长线&下短线
        if (hasSectionLine) {
            [self layer:layer addLineUp:YES andLong:YES andColor:sectionLineColor andBounds:bounds withLeftSpace:0];
        }
        [self layer:layer addLineUp:NO andLong:NO andColor:lineColor andBounds:bounds withLeftSpace:leftSpace];
    } else if (indexPath.row == [self numberOfRowsInSection:indexPath.section] - 1) {
        // 最后一个cell。加下长线
        if (hasSectionLine) {
            [self layer:layer addLineUp:NO andLong:YES andColor:sectionLineColor andBounds:bounds withLeftSpace:0];
        }
    } else {
        // 中间的cell。只加下短线
        [self layer:layer addLineUp:NO andLong:NO andColor:lineColor andBounds:bounds withLeftSpace:leftSpace];
    }
    UIView *testView = [[UIView alloc] initWithFrame:bounds];
    [testView.layer insertSublayer:layer atIndex:0];
    cell.backgroundView = testView;
}

- (void)ty_addLineforPlainCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withLeftSpace:(CGFloat)leftSpace{
    [self ty_addLineforPlainCell:cell forRowAtIndexPath:indexPath withLeftSpace:leftSpace hasSectionLine:YES];
}

- (void)layer:(CALayer *)layer addLineUp:(BOOL)isUp andLong:(BOOL)isLong andColor:(CGColorRef)color andBounds:(CGRect)bounds withLeftSpace:(CGFloat)leftSpace  {
    CALayer *lineLayer = [[CALayer alloc] init];
    CGFloat lineHeight = (1.0f / [UIScreen mainScreen].scale);
    CGFloat left, top;
    if (isUp) {
        top = 0;
    } else {
        top = bounds.size.height-lineHeight;
    }

    if (isLong) {
        left = 0;
    } else {
        left = leftSpace;
    }
    lineLayer.frame = CGRectMake(CGRectGetMinX(bounds)+left, top, bounds.size.width-left, lineHeight);
    lineLayer.backgroundColor = color;
    [layer addSublayer:lineLayer];
}

@end
