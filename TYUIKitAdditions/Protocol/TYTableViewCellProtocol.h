//
//  TYTableViewCellProtocol.h
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/27.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
    Protocol: TableViewCell高度 
 */
@protocol TYTableViewCellProtocol <NSObject>

+ (CGFloat)ty_cellHeight;

@end
