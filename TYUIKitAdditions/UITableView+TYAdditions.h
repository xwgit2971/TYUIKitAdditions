//
//  UITableView+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/26.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (TYAdditions)

// 添加分割线
- (void)ty_addLineforPlainCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withLeftSpace:(CGFloat)leftSpace;
- (void)ty_addLineforPlainCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withLeftSpace:(CGFloat)leftSpace withColor:(UIColor *)color;
- (void)ty_addLineforPlainCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withLeftSpaceAndSectionLine:(CGFloat)leftSpace;
- (void)ty_addLineforPlainCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withLeftSpace:(CGFloat)leftSpace hasSectionLine:(BOOL)hasSectionLine;

@end
