//
//  UIApplication+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (TYAdditions)

// app名称
+ (NSString *)ty_appName;
// app发布版本号
+ (NSString *)ty_appVersion;
// app内部版本号
+ (NSString *)ty_appBuild;
// app的keyWindow
+ (UIWindow *)ty_keyWindow;

@end


@interface UIApplication (TYPermissions)

/**
 * 检查系统"照片"授权状态, 如果权限被关闭, 提示用户去隐私设置中打开.
 */
+ (BOOL)ty_checkPhotoLibraryAuthorizationStatus;

/**
 * 检查系统"相机"授权状态, 如果权限被关闭, 提示用户去隐私设置中打开.
 */
+ (BOOL)ty_checkCameraAuthorizationStatus;

@end
