//
//  UITableViewCell+TYAdditions.m
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/26.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UITableViewCell+TYAdditions.h"

@implementation UITableViewCell (TYAdditions)

+ (NSString *)ty_cellReuseIdentifier {
    return NSStringFromClass([self class]);
}

+ (UINib *)ty_nib {
    return  [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

+ (CGFloat)ty_defaultRowHeight {
    return 44.0;
}

@end
