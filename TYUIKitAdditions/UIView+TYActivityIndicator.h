//
//  UIView+TYActivityIndicator.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
    添加ActivityIndicator功能
 */
@interface UIView (TYActivityIndicator)

@property (nonatomic, strong) UIActivityIndicatorView *ty_activityIndicator;

// 添加
- (void)ty_addActivityIndicator;
- (void)ty_addActivityIndicatorWithStyle:(UIActivityIndicatorViewStyle)activityStyle;
// 移除
- (void)ty_removeActivityIndicator;

@end
