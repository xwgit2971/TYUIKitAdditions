//
//  UIImage+TYAdditions.h
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/11.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TYAdditions)

// 图片限制dataLength大小后的data
- (NSData *)ty_dataSmallerThan:(NSUInteger)dataLength;
// 上传图片限制大小后的data(1024 * 1000)
- (NSData *)ty_dataForUpload;

@end
