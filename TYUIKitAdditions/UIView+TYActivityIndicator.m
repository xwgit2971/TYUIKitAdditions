//
//  UIView+TYActivityIndicator.m
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIView+TYActivityIndicator.h"
#import <objc/runtime.h>

static char TAG_ACTIVITY_INDICATOR;

@implementation UIView (TYActivityIndicator)
@dynamic ty_activityIndicator;

- (UIActivityIndicatorView *)ty_activityIndicator {
    return (UIActivityIndicatorView *)objc_getAssociatedObject(self, &TAG_ACTIVITY_INDICATOR);
}

- (void)setTy_activityIndicator:(UIActivityIndicatorView *)ty_activityIndicator {
    objc_setAssociatedObject(self, &TAG_ACTIVITY_INDICATOR, ty_activityIndicator, OBJC_ASSOCIATION_RETAIN);
}

- (void)ty_addActivityIndicator {
    [self ty_addActivityIndicatorWithStyle:UIActivityIndicatorViewStyleGray];
}

- (void)ty_addActivityIndicatorWithStyle:(UIActivityIndicatorViewStyle) activityStyle {
    if (!self.ty_activityIndicator) {
        self.ty_activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:activityStyle];
        self.ty_activityIndicator.autoresizingMask = UIViewAutoresizingNone;

        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self addSubview:self.ty_activityIndicator];
            [self updateActivityIndicatorPosition];
        });
    }

    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.ty_activityIndicator startAnimating];
    });
}

- (void)updateActivityIndicatorPosition {
    if (self.ty_activityIndicator) {
        self.ty_activityIndicator.center = CGPointMake((CGRectGetWidth(self.frame) / 2.0f), (CGRectGetHeight(self.frame) / 2.0));
    }
}

- (void)ty_removeActivityIndicator {
    if (self.ty_activityIndicator) {
        [self.ty_activityIndicator removeFromSuperview];
        self.ty_activityIndicator = nil;
    }
}

@end
