//
//  UIActionSheet+TYAdditions.h
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/11/1.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActionSheet (TYAdditions)

// ActionSheet构造器
+ (instancetype)ty_actionSheetCustomWithTitle:(NSString *)title buttonTitles:(NSArray *)buttonTitles destructiveTitle:(NSString *)destructiveTitle cancelTitle:(NSString *)cancelTitle andDidDismissBlock:(void (^)(UIActionSheet *sheet, NSInteger index))block;

@end
