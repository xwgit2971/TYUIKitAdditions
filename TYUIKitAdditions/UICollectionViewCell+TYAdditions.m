//
//  UICollectionViewCell+TYAdditions.m
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/19.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UICollectionViewCell+TYAdditions.h"

@implementation UICollectionViewCell (TYAdditions)

+ (NSString *)ty_cellReuseIdentifier {
    return NSStringFromClass([self class]);
}

+ (UINib *)ty_nib {
    return  [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

@end
