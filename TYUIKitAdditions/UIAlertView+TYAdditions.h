//
//  UIAlertView+TYAdditions.h
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/27.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (TYAdditions)

// "我知道了"提示框
+ (void)ty_showAlertTip:(NSString *)msg;

@end
