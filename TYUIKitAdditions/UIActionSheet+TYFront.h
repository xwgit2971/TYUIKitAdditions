//
//  UIActionSheet+TYFront.h
//  Coding_iOS
//
//  Created by Ease on 15/10/13.
//  Copyright © 2015年 Coding. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActionSheet (TYFront)

@end

// ActionSheet Swizzle
void swizzleAllActionSheet();
