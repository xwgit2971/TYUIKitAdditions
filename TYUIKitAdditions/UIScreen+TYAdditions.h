//
//  UIScreen+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (TYAdditions)

// 屏幕宽高
+ (CGRect)ty_bounds;
// 屏幕宽
+ (CGFloat)ty_width;
// 屏幕高
+ (CGFloat)ty_height;
// 1像素
+ (CGFloat)ty_1px;
// 依iPhone5设计比例下的x
+ (CGFloat)ty_scaleFromiPhone5Design:(CGFloat)x;
// 减去状态栏的高度
+ (CGRect)ty_frameWithoutStatusBar;
// 减去状态栏、导航栏的高度
+ (CGRect)ty_frameWithoutStatusBarAndNav;

@end
