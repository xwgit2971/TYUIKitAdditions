//
//  UIView+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TYAdditions)

// 双倍size
- (CGSize)ty_doubleSizeOfFrame;
// 移除视图
- (void)ty_removeViewWithTag:(NSInteger)tag;

@end


@interface UIView (TYNib)

+ (instancetype)ty_viewFromNib;

@end


@interface UIView (TYLayer)

@property (nonatomic) IBInspectable UIColor *ty_borderColor;
@property (nonatomic) IBInspectable CGFloat ty_borderWidth;
@property (nonatomic) IBInspectable CGFloat ty_cornerRadius;
@property (nonatomic) IBInspectable BOOL ty_masksToBounds;

- (void)ty_doCircleFrame;
- (void)ty_doNotCircleFrame;
- (void)ty_doBorderWidth:(CGFloat)width color:(UIColor *)color cornerRadius:(CGFloat)cornerRadius;
- (void)ty_addRoundingCorners:(UIRectCorner)corners cornerRadii:(CGSize)cornerRadii;
- (void)ty_addGradientLayerWithColors:(NSArray *)cgColorArray locations:(NSArray *)floatNumArray startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;

@end


@interface UIView (TYScroll)

// subViews中的UITableView
- (UIView *)ty_scrollViewContainer;
- (void)ty_setSubScrollsToTop:(BOOL)scrollsToTop;

@end


@interface UIView (TYViewController)

- (UIViewController *)ty_findViewController;

@end


@interface UIView (TYAnimation)

+ (UIViewAnimationOptions)ty_animationOptionsForCurve:(UIViewAnimationCurve)curve;

@end


@interface UIView (TYLine)

// 添加直线
- (void)ty_addLineUp:(BOOL)hasUp andDown:(BOOL)hasDown;
- (void)ty_addLineUp:(BOOL)hasUp andDown:(BOOL)hasDown andColor:(UIColor *)color;
- (void)ty_addLineUp:(BOOL)hasUp andDown:(BOOL)hasDown andLeftSpace:(CGFloat)leftSpace;
- (void)ty_addLineUp:(BOOL)hasUp andDown:(BOOL)hasDown andColor:(UIColor *)color andLeftSpace:(CGFloat)leftSpace;

// 直线构造器
+ (UIView *)ty_lineViewWithPointYY:(CGFloat)pointY;
+ (UIView *)ty_lineViewWithPointYY:(CGFloat)pointY andLeftSpace:(CGFloat)leftSpace;
+ (UIView *)ty_lineViewWithPointYY:(CGFloat)pointY andColor:(UIColor *)color;
+ (UIView *)ty_lineViewWithPointYY:(CGFloat)pointY andColor:(UIColor *)color andLeftSpace:(CGFloat)leftSpace;

@end


@interface UIView (TYDebug)

// 输出子视图的目录树
- (void)ty_outputSubviewTree;
// 输出某个View的subview目录
+ (void)ty_outputTreeInView:(UIView *)view withSeparatorCount:(NSInteger)count;

@end
