//
//  UITableViewCell+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/26.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (TYAdditions)

// 复用Identifier
+ (NSString *)ty_cellReuseIdentifier;
// 生成nib
+ (UINib *)ty_nib;
// 默认行高(44.0)
+ (CGFloat)ty_defaultRowHeight;

@end
