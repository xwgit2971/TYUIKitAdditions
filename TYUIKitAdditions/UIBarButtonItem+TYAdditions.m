//
//  UIBarButtonItem+TYAdditions.m
//  TYUIKitAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIBarButtonItem+TYAdditions.h"

@implementation UIBarButtonItem (TYAdditions)

+ (UIBarButtonItem *)ty_itemWithTitle:(NSString *)title target:(id)target action:(SEL)action {
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
    return item;
}

+ (UIBarButtonItem *)ty_itemWithTitle:(NSString *)title color:(UIColor *)color target:(id)target action:(SEL)action {
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
    [item setTitleTextAttributes:@{NSForegroundColorAttributeName: color} forState:UIControlStateNormal];
    return item;
}

+ (UIBarButtonItem *)ty_itemWithImage:(UIImage *)image target:(id)target action:(SEL)action {
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:target action:action];
    return item;
}

@end
