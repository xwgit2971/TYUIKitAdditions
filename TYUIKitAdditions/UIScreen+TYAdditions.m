//
//  UIScreen+TYAdditions.m
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIScreen+TYAdditions.h"

@implementation UIScreen (TYAdditions)

+ (CGRect)ty_bounds {
    return [UIScreen mainScreen].bounds;
}

+ (CGFloat)ty_width {
    return [UIScreen mainScreen].bounds.size.width;
}

+ (CGFloat)ty_height {
    return [UIScreen mainScreen].bounds.size.height;
}

+ (CGFloat)ty_1px {
    return 1 / [UIScreen mainScreen].scale;
}

+ (CGFloat)ty_scaleFromiPhone5Design:(CGFloat)x {
    return x * ([self ty_width]/320);
}

+ (CGRect)ty_frameWithoutStatusBar {
    CGRect frame = [self ty_bounds];
    frame.size.height -= 20;
    return frame;
}

+ (CGRect)ty_frameWithoutStatusBarAndNav {
    CGRect frame = [self ty_bounds];
    frame.size.height -= (20+44);
    return frame;
}
@end
