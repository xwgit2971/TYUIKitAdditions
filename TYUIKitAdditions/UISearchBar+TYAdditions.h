//
//  UISearchBar+TYAdditions.h
//  TYUserDefaults
//
//  Created by 夏伟 on 16/9/23.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (TYAdditions)

// 添加背景色
- (void)ty_insertBGColor:(UIColor *)backgroundColor;
// 获取输入框
- (UITextField *)ty_TextField;

@end
