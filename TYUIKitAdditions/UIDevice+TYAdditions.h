//
//  UIDevice+TYAdditions.h
//  TYFoundationAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (TYAdditions)

// 是否iphone4
+ (BOOL)ty_iphone4;
// 是否iphone5
+ (BOOL)ty_iphone5;
// 是否iphone6
+ (BOOL)ty_iphone6;
// 是否iphone6Plus
+ (BOOL)ty_iphone6Plus;
// 是否iPad
- (BOOL)ty_isPad;

// 是否是Retain屏
+ (BOOL)ty_isRetain;

// 系统相关信息
+ (NSDictionary *)ty_systemInfoDict;
// 设备类型
- (NSString *)ty_deviceName;
// 设备名称
- (NSString *)ty_iosModel;

// MAC地址
- (NSString *)ty_macaddress;
// MAC地址MD5
- (NSString *)ty_macaddressMD5;
// 系统版本
- (NSString *)ty_ostype;

// 主机名称
- (NSString *)ty_hostName;
// IP地址
- (NSString *)ty_localIPAddress;
// 磁盘总容量
- (NSString *)ty_totalDiskspace;
// 磁盘可用容量
- (NSString *)ty_freeDiskspace;
// 电池状态
- (UIDeviceBatteryState)ty_batteryState;

@end
