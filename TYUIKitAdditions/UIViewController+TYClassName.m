//
//  UIViewController+TYClassName.m
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/12/15.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIViewController+TYClassName.h"
#import <objc/runtime.h>

#define kTagClassName 2000

@implementation UIViewController (TYClassName)

#pragma mark - Lifecycle
#ifdef DEBUG
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];

        SEL originalSelector = @selector(viewDidAppear:);
        SEL swizzledSelector = @selector(ty_viewDidAppear:);

        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);

        BOOL didAddMethod = class_addMethod(class,
                                            originalSelector,
                                            method_getImplementation(swizzledMethod),
                                            method_getTypeEncoding(swizzledMethod));

        if (didAddMethod) {
            class_replaceMethod(class,
                                swizzledSelector,
                                method_getImplementation(originalMethod),
                                method_getTypeEncoding(originalMethod));
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
}

#pragma mark - Method Swizzling
- (void)ty_viewDidAppear:(BOOL)animated {
    [self ty_viewDidAppear:animated];

    [[self class] displayClassName];
}

#pragma mark - Views
+ (void)displayClassName {
    UIWindow *window = [self appWindow];

    UILabel *classNameLabel;
    if ([window viewWithTag:kTagClassName]) {
        classNameLabel = (UILabel *)[window viewWithTag:kTagClassName];
        [window bringSubviewToFront:classNameLabel];
    } else {
        classNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 15, window.bounds.size.width, 20)];
        classNameLabel.textColor = [UIColor redColor];
        classNameLabel.font = [UIFont systemFontOfSize:12];
        classNameLabel.tag = kTagClassName;
        [window addSubview:classNameLabel];
        [window bringSubviewToFront:classNameLabel];
    }

    const char *className = class_getName(self.class);
    NSString *classNameStr = @(className);

    if ([self needDisplay:classNameStr]) {
        [classNameLabel setText:classNameStr];
    }
}

+ (void)removeClassName {
    UIWindow *window = [self appWindow];

    UILabel *classNameLabel;
    if ([window viewWithTag:kTagClassName]) {
        classNameLabel = (UILabel *)[window viewWithTag:kTagClassName];
        [classNameLabel removeFromSuperview];
    }
}

+ (BOOL)needDisplay:(NSString *)className {
    if ([className isEqualToString:@"UIInputWindowController"]) {
        return NO;
    } else if ([className isEqualToString:@"UINavigationController"]) {
        return NO;
    } else if ([className isEqualToString:@"UICompatibilityInputViewController"]) {
        return NO;
    } else {
        return YES;
    }
}

+ (UIWindow *)appWindow {
    id<UIApplicationDelegate> appDelegate = [UIApplication sharedApplication].delegate;
    return [appDelegate window];
}
#endif

@end
