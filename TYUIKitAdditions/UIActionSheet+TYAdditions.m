//
//  UIActionSheet+TYAdditions.m
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/11/1.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIActionSheet+TYAdditions.h"
#import <BlocksKit/UIActionSheet+BlocksKit.h>

@implementation UIActionSheet (TYAdditions)

+ (instancetype)ty_actionSheetCustomWithTitle:(NSString *)title buttonTitles:(NSArray *)buttonTitles destructiveTitle:(NSString *)destructiveTitle cancelTitle:(NSString *)cancelTitle andDidDismissBlock:(void (^)(UIActionSheet *sheet, NSInteger index))block {
    UIActionSheet *actionSheet = [UIActionSheet bk_actionSheetWithTitle:title];
    if (buttonTitles && buttonTitles.count > 0) {
        for (NSString *buttonTitle in buttonTitles) {
            [actionSheet bk_addButtonWithTitle:buttonTitle handler:nil];
        }
    }
    if (destructiveTitle) {
        [actionSheet bk_setDestructiveButtonWithTitle:destructiveTitle handler:nil];
    }
    if (cancelTitle) {
        [actionSheet bk_setCancelButtonWithTitle:cancelTitle handler:nil];
    }
    [actionSheet bk_setDidDismissBlock:block];
    return actionSheet;
}

@end
