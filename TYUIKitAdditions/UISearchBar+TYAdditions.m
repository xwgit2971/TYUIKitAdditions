//
//  UISearchBar+TYAdditions.m
//  TYUserDefaults
//
//  Created by 夏伟 on 16/9/23.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UISearchBar+TYAdditions.h"
#import <FrameAccessor/FrameAccessor.h>
#import <UIImage+Additions/UIImage+Additions.h>

@implementation UISearchBar (TYAdditions)

- (void)ty_insertBGColor:(UIColor *)backgroundColor {
    static NSInteger customBgTag = 999;
    UIView *realView = [[self subviews] firstObject];
    [[realView subviews] enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        if (obj.tag == customBgTag) {
            [obj removeFromSuperview];
        }
    }];
    if (backgroundColor) {
        UIImageView *customBg = [[UIImageView alloc] initWithImage:[UIImage add_imageWithColor:backgroundColor size:CGSizeMake(CGRectGetWidth(self.frame), CGRectGetHeight(self.frame) + 20)]];
        [customBg setY:-20];
        customBg.tag = customBgTag;
        [[[self subviews] firstObject] insertSubview:customBg atIndex:1];
    }
}

- (UITextField *)ty_TextField {
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(UIView *candidateView, NSDictionary *bindings) {
        return [candidateView isMemberOfClass:NSClassFromString(@"UISearchBarTextField")];
    }];
    return [self.subviews.firstObject.subviews filteredArrayUsingPredicate:predicate].lastObject;
}

@end
