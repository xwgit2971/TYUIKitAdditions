//
//  UIBarButtonItem+TYAdditions.h
//  TYUIKitAdditions
//
//  Created by 夏伟 on 16/9/25.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (TYAdditions)

// BarButtonItem构造器
+ (UIBarButtonItem *)ty_itemWithTitle:(NSString *)title target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)ty_itemWithTitle:(NSString *)title color:(UIColor *)color target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)ty_itemWithImage:(UIImage *)image target:(id)target action:(SEL)action;

@end
