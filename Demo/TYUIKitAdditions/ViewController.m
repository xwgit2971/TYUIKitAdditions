//
//  ViewController.m
//  MMUIKitAdditions
//
//  Created by 夏伟 on 2016/10/8.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import "TYUIKitAdditions.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *btn;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)btnClicked:(id)sender {
    [self.btn ty_addActivityIndicator];
}

@end
