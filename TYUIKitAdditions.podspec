Pod::Spec.new do |s|
  s.name = 'TYUIKitAdditions'
  s.version = '0.1.0'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'Additional categories for the UIKit framework'
  s.homepage = 'https://gitlab.com/xwgit2971/TYUIKitAdditions'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYUIKitAdditions.git', :tag => s.version }
  s.source_files = 'TYUIKitAdditions/*.{h,m}', 'TYUIKitAdditions/Protocol/*.{h,m}'
  s.framework = 'UIKit'
  s.requires_arc = true
  s.dependency  "FrameAccessor", '~> 2.0'
  s.dependency  "UIImage+Additions", '~> 2.1.1'
  s.dependency  "BlocksKit", '~> 2.2.5'
  s.dependency  "TYFoundationAdditions"
  s.dependency  "UIColor-TYAdditions"
end
